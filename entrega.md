## Parte Básica
* He añadido mi registered.json con las 3 maquinas registradas en la captura 2.
* He tenido que editar el fichero simplertp.py para que el serverrtp.py escribiera por pantalla lo que tiene que escribir según los históricos. Por eso he añadido el simplertp.py en un push.
* En la parte final de la comunicación, cuando el servidorRTP envia el tráfico RTP a cliente, y tiene que terminar cuando le llegue un BYE, en mi caso, termina a los 10 segundos ya que no he sido capaz de hacerlo cuando tiene que hacerlo (despues de intentarlo mucho) que es cuando le llega un BYE. Es decir, el tráfico RTP termina a los 10 segundos y el cliente también. Por eso en las capturas de wireshark puede que llegue el ultimo paquete RTP mientras se está enviando el BYE y el 200OK final.
  * Esta parte de mi codigo:
    - if method[0] == "ACK":
      - self.sender = simplertp.RTPSender(ip=self.dicc["ip"], port=27138, file=fichero, printout=True)
      - self.sender.send_threaded()
      - time.sleep(10)
      - self.sender.finish()
      - if method[0] == 'BYE':
    - method = 'SIP/2.0 200 OK'
      - self.socket.sendto(bytes(method + '\r\n\r\n', 'utf-8'), ('0.0.0.0', 6001))
      - escribir_envio_sip(method, ip, puerto)
  * Deberia ser asi:
    - if method[0] == "ACK":
      - self.sender = simplertp.RTPSender(ip=self.dicc["ip"], port=27138, file=fichero, printout=True)
      - self.sender.send_threaded()
    - if method[0] == 'BYE':
      - method = 'SIP/2.0 200 OK'
      - self.sender.finish()
      - self.socket.sendto(bytes(method + '\r\n\r\n', 'utf-8'), ('0.0.0.0', 6001))
      - escribir_envio_sip(method, ip, puerto)
  * Pero me daba error en el simplertp que tenia que tener un valor self asignado y no entendí el porqué.
* Los ficheros se ejecutan igual que en el enunciado.

## Parte adicional
* Cabecera de tamaño
  * En los paquetes SIP/SDP el tamaño del cuerpo del mensaje esta programado, lo unico que no se muestra por respetar los históricos. En esta linea se puede ver: size_cuerpo = sys.getsizeof(version + info_usuario_sdp + session + tiempo + enviar_multimedia). Se puede ver el tamaño del cuerpo del mensaje ejecutando con Debug en el pycharm.
* Gestion de errores
  * Se ha añadido la función de gestion de errores SIP: el programa manda el fallo oportuno si la petición SIP no esta bien formada.


