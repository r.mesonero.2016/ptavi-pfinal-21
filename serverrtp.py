#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import sys
import socket
import os
import simplertp
import time
import re


# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'
PORT = 6001
fichero = sys.argv[2]
IP = '127.0.0.1'


def escribir_envio_sip(peticion, ip, puerto):
    tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
    method = peticion.split(" ")
    string = re.sub("\r\n", "", method[2])
    method.pop(2)
    method.append(string)
    imprimir = " ".join(method)
    if method[0] == 'REGISTER' or 'SIP/2.0' or 'INVITE' or 'ACK' or 'BYE':
        print(tiempo + ' SIP to ' + ip + ':' + str(puerto) + ": " + imprimir)


def escribir_recibo_sip(peticion, ip, puerto):
    tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
    method = peticion.split(" ")
    string = re.sub("\r\n", "", method[2])
    method.pop(2)
    method.append(string)
    imprimir = " ".join(method)
    if method[0] == 'REGISTER' or method[0] == 'SIP/2.0' or method[0] == 'INVITE' \
            or method[0] == 'ACK' or method[0] == 'BYE':
        print(tiempo + ' SIP from ' + ip + ':' + str(puerto) + ": " + imprimir)


def escribir_envio_rtp(ip, puerto):
    tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
    print(tiempo + ' RTP to ' + ip + ':' + str(puerto))


def register(rtp_port):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))
            # Enviamos al servidor:
            msg = 'REGISTER ' + 'sip:cancion@signasong.net' + ' SIP/2.0'
            my_socket.send(bytes(msg, 'utf-8') + b'\r\n\r\n')
            escribir_envio_sip(msg, IP, PORT)
            # Recibimos del servidor
            data = my_socket.recv(1024).decode('utf-8')
            escribir_recibo_sip(data, IP, PORT)
    except ConnectionRefusedError:
        print("Error conectando a servidor")


class SIPHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    dicc = {}
    sender = simplertp.RTPSender

    def handle(self):

        string = ""
        # Recoge los datos del cliente:
        for line in self.rfile:
            string = string + line.decode('utf-8')
        ip = self.client_address[0]
        puerto = self.client_address[1]
        paraimprimir = string.split('\r\n')
        peticion = paraimprimir[0]
        peticionsip = peticion.split(" ")
        if peticionsip[0] == 'INVITE' or peticionsip[0] == 'ACK' or peticionsip[0] == 'BYE':
            escribir_recibo_sip(peticion, ip, puerto)
        method = string.split(" ")
        if method[0] == "INVITE":
            lista = string.split("\r\n")
            peticion = lista[0]
            lista_peticion = peticion.split(" ")
            registro_nombre = lista_peticion[1].strip('sip:')
            if lista_peticion[0] == "INVITE":
                descripcion_sdp = lista[1:10]
                descripcion_sdp_correcta = []
                for string in descripcion_sdp:
                    if string != "":
                        descripcion_sdp_correcta.append(string)
                content_type = descripcion_sdp_correcta[0]
                content_length = descripcion_sdp_correcta[1].split(" ")
                cabecera_longitud = content_length[0] + " "
                version = descripcion_sdp_correcta[2]
                info_usuario_sdp = "o=heyjude@singason.net"
                info_sdp = info_usuario_sdp.split(" ")
                usuario_sdp = info_sdp[0][2:]
                self.dicc["usuario"] = usuario_sdp
                self.dicc["ip"] = ip
                session = descripcion_sdp_correcta[4]
                tiempo = descripcion_sdp_correcta[5]
                info_multimedia_sdp = descripcion_sdp_correcta[6]
                info_multimedia = info_multimedia_sdp.split(" ")
                puerto_rtp_usuario = info_multimedia[1]
                self.dicc["puerto"] = puerto_rtp_usuario
                puerto_rtp_server = '27138'
                info_multimedia[1] = puerto_rtp_server
                enviar_multimedia = " ".join(info_multimedia)
                size_cuerpo = sys.getsizeof(version + info_usuario_sdp + session + tiempo + enviar_multimedia)
                self.socket.sendto(bytes('SIP/2.0 200 OK' + '\r\n', 'utf-8') +
                                   bytes(content_type + '\r\n', 'utf-8') +
                                   bytes(cabecera_longitud + str(size_cuerpo) + '\r\n\r\n', 'utf-8') +
                                   bytes(version + '\r\n', 'utf-8') +
                                   bytes(info_usuario_sdp + '\r\n', 'utf-8') +
                                   bytes(session + '\r\n', 'utf-8') +
                                   bytes(tiempo + '\r\n', 'utf-8') +
                                   bytes(enviar_multimedia + '\r\n', 'utf-8'), ('0.0.0.0', 6001))
                escribir_envio_sip('SIP/2.0 200 OK', ip, puerto)
        if method[0] == "ACK":
            self.sender = simplertp.RTPSender(ip=self.dicc["ip"], port=27138, file=fichero, printout=True)
            self.sender.send_threaded()
            time.sleep(10)
            self.sender.finish()
        if method[0] == 'BYE':
            method = 'SIP/2.0 200 OK'
            self.socket.sendto(bytes(method + '\r\n\r\n', 'utf-8'), ('0.0.0.0', 6001))
            escribir_envio_sip(method, ip, puerto)


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    # Definimos los argumentos que se utilizan para ejecutar el cliente
    try:
        # queda limitar los sip que esten bien formados con @
        datos_servidor_sip = sys.argv[1]
        ip_puerto = datos_servidor_sip.split(':')
        ip_servidor_sip = ip_puerto[0]
        port_servidor_sip = ip_puerto[1]

        fichero = sys.argv[2]
        if os.path.isfile("/home/alumnos/rmesoner/Escritorio/PTAVI2/ptavi-pfinal-21/cancion.mp3"):
            pass
        else:
            print("No existe el fichero")
            exit()
        lista1 = fichero.split('.')
        user_servidorrtp_sip = lista1[0]

    except IndexError:
        sys.exit("Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <file>")

    try:
        with socketserver.UDPServer(('0.0.0.0', 6002), SIPHandler) as serv:
            tiempo = time.strftime("%Y%m%d%H%M%S",
                                   time.gmtime(int(time.time()) + 3600))
            print(tiempo + " Starting...")
            rtp_port = serv.server_address[1]
            register(rtp_port)
            serv.serve_forever()
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")


if __name__ == "__main__":
    main()
