#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import os
import time
import re


PORT = 6001


def escribir_envio_sip(peticion, ip, puerto):
    tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
    method = peticion.split(" ")
    string = re.sub("\r\n", "", method[2])
    method.pop(2)
    method.append(string)
    imprimir = " ".join(method)
    if method[0] == 'REGISTER' or 'SIP/2.0' or 'INVITE' or 'ACK' or 'BYE':
        print(tiempo + ' SIP to ' + ip + ':' + str(puerto) + ": " + imprimir)


def escribir_recibo_sip(peticion, ip, puerto):
    tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
    method = peticion.split(" ")
    string = re.sub("\r\n", "", method[2])
    method.pop(2)
    method.append(string)
    imprimir = " ".join(method)
    if method[0] == 'REGISTER' or method[0] == 'SIP/2.0' or method[0] == 'INVITE'\
            or method[0] == 'ACK' or method[0] == 'BYE':
        print(tiempo + ' SIP from ' + ip + ':' + str(puerto) + ": " + imprimir)


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    lista_json = []
    dicc = {}
    waiting = []
    puerto_rtp_serverrtp = 0
    bye = bool

    def handle(self):
        """
        lee la peticion del cliente y prepara la lista en el formato adecuado
        """
        # Lee del json si existiera:
        if os.path.exists('registered.json'):
            self.json2register()
        tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
        string = ""
        # Recoge los datos del cliente:
        for line in self.rfile:
            string = string + line.decode('utf-8')
        ip = self.client_address[0]
        puerto = self.client_address[1]
        method = string.split(" ")
        paraimprimir = string.split('\r\n')
        peticion = paraimprimir[0]
        peticionsip = peticion.split(" ")
        if peticionsip[0] == 'REGISTER' or peticionsip[0] == 'INVITE' or peticionsip[0] == 'ACK' \
                or peticionsip[0] == 'BYE' or peticionsip[0] == 'SIP/2.0':
            escribir_recibo_sip(peticion, ip, puerto)
        # Envia al cliente el OK:
        if method[0] == "REGISTER":
            lista = string.split(" ")
            # Ordena los datos para crear mi lista:
            peticion = lista[0]
            lista_peticion = peticion.split(" ")
            registro_nombre = lista[1].strip('sip:')
            atributos_json = {}
            atributos_json["address"] = ip
            atributos_json["puerto"] = puerto
            self.puerto_rtp_serverrtp = lista[2]
            self.lista_json.append(registro_nombre)
            self.lista_json.append(atributos_json)
            self.register2json()
            self.waiting.append(self.client_address)
            self.bye = False
            if lista[2] != "SIP/2.0\r\n\r\n":
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                escribir_envio_sip('SIP/2.0 400 Bad Request', ip, puerto)
            elif len(lista) != 3:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                escribir_envio_sip('SIP/2.0 400 Bad Request', ip, puerto)
            elif lista[1][0:3] == 'sip:':
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                escribir_envio_sip('SIP/2.0 400 Bad Request', ip, puerto)
            else:
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                escribir_envio_sip('SIP/2.0 200 OK', ip, puerto)
        elif method[0] == "INVITE":
            lista = string.split("\r\n")
            peticion = lista[0]
            lista_peticion = peticion.split(" ")
            registro_nombre = lista_peticion[1].strip('sip:')
            if lista_peticion[0] == "INVITE":
                descripcion_sdp = lista[1:10]
                descripcion_sdp_correcta = []
                for string in descripcion_sdp:
                    if string != "":
                        descripcion_sdp_correcta.append(string)
                content_type = descripcion_sdp_correcta[0]
                content_length = descripcion_sdp_correcta[1].split(" ")
                cabecera_longitud = content_length[0] + " "
                version = descripcion_sdp_correcta[2]
                info_usuario_sdp = descripcion_sdp_correcta[3]
                info_sdp = info_usuario_sdp.split(" ")
                usuario_sdp = info_sdp[0][2:]
                self.dicc["usuario"] = usuario_sdp
                self.dicc["ip"] = ip
                session = descripcion_sdp_correcta[4]
                tiempo = descripcion_sdp_correcta[5]
                info_multimedia_sdp = descripcion_sdp_correcta[6]
                info_multimedia = info_multimedia_sdp.split(" ")
                puerto_rtp_usuario = info_multimedia[1]
                self.dicc["puerto"] = puerto_rtp_usuario
                puerto_rtp_server = '7080'
                info_multimedia[1] = puerto_rtp_server
                enviar_multimedia = " ".join(info_multimedia)
                size_cuerpo = sys.getsizeof(version + info_usuario_sdp + session + tiempo + enviar_multimedia)
                self.waiting.append(self.client_address)
                self.socket.sendto(bytes(peticion + '\r\n', 'utf-8') +
                                   bytes(content_type + '\r\n', 'utf-8') +
                                   bytes(cabecera_longitud + str(size_cuerpo) + '\r\n\r\n', 'utf-8') +
                                   bytes(version + '\r\n', 'utf-8') +
                                   bytes(info_usuario_sdp + '\r\n', 'utf-8') +
                                   bytes(session + '\r\n', 'utf-8') +
                                   bytes(tiempo + '\r\n', 'utf-8') +
                                   bytes(enviar_multimedia + '\r\n', 'utf-8'), ('0.0.0.0', 6002))
                escribir_envio_sip(peticion, ip, puerto)
            elif lista_peticion[2] != "SIP/2.0":
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                escribir_envio_sip('SIP/2.0 400 Bad Request', ip, puerto)
            elif len(lista_peticion) != 3:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                escribir_envio_sip('SIP/2.0 400 Bad Request', ip, puerto)
            elif lista_peticion[1][0:3] == 'sip:':
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                escribir_envio_sip('SIP/2.0 400 Bad Request', ip, puerto)
        elif method[0] == "SIP/2.0":
            if len(self.waiting) == 3:
                lista = string.split("\r\n")
                peticion = lista[0]
                lista_peticion = peticion.split(" ")
                if lista_peticion[0] == "SIP/2.0":
                    descripcion_sdp = lista[1:10]
                    descripcion_sdp_correcta = []
                    for string in descripcion_sdp:
                        if string != "":
                            descripcion_sdp_correcta.append(string)
                    content_type = descripcion_sdp_correcta[0]
                    content_length = descripcion_sdp_correcta[1].split(" ")
                    cabecera_longitud = content_length[0] + " "
                    version = descripcion_sdp_correcta[2]
                    info_usuario_sdp = descripcion_sdp_correcta[3]
                    info_sdp = info_usuario_sdp.split(" ")
                    usuario_sdp = info_sdp[0][2:]
                    self.dicc["usuario"] = usuario_sdp
                    self.dicc["ip"] = ip
                    session = descripcion_sdp_correcta[4]
                    tiempo = descripcion_sdp_correcta[5]
                    info_multimedia_sdp = descripcion_sdp_correcta[6]
                    info_multimedia = info_multimedia_sdp.split(" ")
                    puerto_rtp_usuario = info_multimedia[1]
                    self.dicc["puerto"] = puerto_rtp_usuario
                    puerto_rtp_server = '27138'
                    info_multimedia[1] = puerto_rtp_server
                    enviar_multimedia = " ".join(info_multimedia)
                    size_cuerpo = sys.getsizeof(version + info_usuario_sdp + session + tiempo + enviar_multimedia)
                    self.waiting.append(self.client_address)
                    self.socket.sendto(bytes(peticion + '\r\n', 'utf-8') +
                                       bytes(content_type + '\r\n', 'utf-8') +
                                       bytes(cabecera_longitud + str(size_cuerpo) + '\r\n\r\n', 'utf-8') +
                                       bytes(version + '\r\n', 'utf-8') +
                                       bytes(info_usuario_sdp + '\r\n', 'utf-8') +
                                       bytes(session + '\r\n', 'utf-8') +
                                       bytes(tiempo + '\r\n', 'utf-8') +
                                       bytes(enviar_multimedia + '\r\n', 'utf-8'), ('0.0.0.0', 6003))
                    escribir_envio_sip('SIP/2.0 200 OK', ip, 6003)
            else:
                method = 'SIP/2.0 200 OK'
                self.waiting.append(self.client_address)
                self.socket.sendto(bytes(method + '\r\n', 'utf-8'), ('0.0.0.0', 6003))
                escribir_envio_sip('SIP/2.0 200 OK', ip, 6003)
        elif method[0] == "ACK":
            method = 'ACK ' + peticionsip[1] + ' SIP/2.0'
            self.waiting.append(self.client_address)
            self.socket.sendto(bytes(method + '\r\n', 'utf-8'), ('0.0.0.0', 6002))
            escribir_envio_sip(method, ip, 6002)
        elif method[0] == "BYE":
            method = 'BYE ' + peticionsip[1] + ' SIP/2.0'
            self.waiting.append(self.client_address)
            self.socket.sendto(bytes(method + '\r\n', 'utf-8'), ('0.0.0.0', 6002))
            self.bye = True
            escribir_envio_sip(method, ip, 6002)

    def register2json(self):
        """
        crea el json con el formato que tiene la lista
        """
        with open('registered.json', 'w') as jsonfile:
            json.dump(self.lista_json, jsonfile, indent=4)

    def json2register(self):
        """
        abre el json y los datos los carga en la lista
        """
        try:
            with open('registered.json', 'r') as jsonfile:
                self.lista_json = json.load(jsonfile)
        except():
            pass


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
    print(tiempo + ' Starting...')
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
