#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socketserver
import sys
import socket
import simplertp
import time
import threading
import re

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'
PORT = 6001
filename = sys.argv[5]
RTP_PORT_CLIENTE = '7080'
TIEMPO = sys.argv[4]
usuario_sip = sys.argv[2]


def escribir_envio_sip(peticion, ip, puerto):
    tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
    method = peticion.split(" ")
    string = re.sub("\r\n", "", method[2])
    method.pop(2)
    method.append(string)
    imprimir = " ".join(method)
    if method[0] == 'REGISTER' or 'SIP/2.0' or 'INVITE' or 'ACK' or 'BYE':
        print(tiempo + ' SIP to ' + ip + ':' + str(puerto) + ": " + imprimir)


def escribir_recibo_sip(peticion, ip, puerto):
    tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
    method = peticion.split(" ")
    string = re.sub("\r\n", "", method[2])
    method.pop(2)
    method.append(string)
    imprimir = " ".join(method)
    if method[0] == 'REGISTER' or method[0] == 'SIP/2.0' or method[0] == 'INVITE'\
            or method[0] == 'ACK' or method[0] == 'BYE':
        print(tiempo + ' SIP from ' + ip + ':' + str(puerto) + ": " + imprimir)


def escribir_recibo_rtp(ip, puerto):
    tiempo = time.strftime("%Y%m%d%H%M%S", time.gmtime(int(time.time()) + 3600))
    print(tiempo + ' RTP from ' + ip + ':' + str(puerto))


def register(rtp_port):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))
            # Enviamos al servidor:
            msg = 'REGISTER ' + usuario_sip + ' SIP/2.0'
            my_socket.send(bytes(msg, 'utf-8') + b'\r\n\r\n')
            escribir_envio_sip(msg, '127.0.0.1', PORT)
            # Recibimos del servidor
            data = my_socket.recv(1024).decode('utf-8')
            escribir_recibo_sip(data, '127.0.0.1', PORT)
            method = 'invite'
            cabecera_content_type = 'Content-Type: application/sdp'
            cabecera_longitud = 'Content-Length: '
            version = "0"
            origen = usuario_sip
            nombre_sesion = "misesion"
            tiempo = "0"
            rtp_port = RTP_PORT_CLIENTE
            multimedia = "audio " + rtp_port + " RTP"
            size_cuerpo = sys.getsizeof("v=" + version + '\r\n' + "o=" + origen + '\r\n' +
                                        "s=" + nombre_sesion + '\r\n' + "t=" + tiempo + '\r\n' +
                                        "m=" + multimedia + '\r\n')
            my_socket.send(bytes(method.upper() + " " + usuario_sip + ' SIP/2.0' + '\r\n', 'utf-8') +
                           bytes(cabecera_content_type + '\r\n', 'utf-8') +
                           bytes(cabecera_longitud + str(size_cuerpo) + '\r\n', 'utf-8') + b'\r\n\r\n' +
                           bytes('v=' + version + '\r\n', 'utf-8') +
                           bytes('o=' + origen + '\r\n', 'utf-8') +
                           bytes('s=' + nombre_sesion + '\r\n', 'utf-8') +
                           bytes('t=' + tiempo + '\r\n', 'utf-8') +
                           bytes('m=' + multimedia + '\r\n', 'utf-8') + b'\r\n\r\n')
            escribir_envio_sip(method.upper() + " " + usuario_sip + ' SIP/2.0', '127.0.0.1', PORT)
    except ConnectionRefusedError:
        print("Error conectando a servidor")


def despedirse():
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))
            # Enviamos al servidor:
            msg = 'BYE ' + usuario_sip + ' SIP/2.0'
            my_socket.send(bytes(msg, 'utf-8') + b'\r\n\r\n')
            escribir_envio_sip(msg, '127.0.0.1', PORT)
            # Recibimos del servidor
            data = my_socket.recv(1024).decode('utf-8')
    except ConnectionRefusedError:
        print("Error conectando a servidor")


class RTPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        ip = self.client_address[0]
        port = self.client_address[1]
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        escribir = ""
        escribir_recibo_rtp(ip, port)

    @classmethod
    def open_output(cls, filename):
        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def recibir_rtp():
    RTPHandler.open_output(filename)
    with socketserver.UDPServer(('127.0.0.1', 27138), RTPHandler) as serv2:
        threading.Thread(target=serv2.serve_forever).start()
        time.sleep(int(TIEMPO))
        serv2.shutdown()
    # Cerramos el fichero donde estamos escribiendo los datos recibidos
    RTPHandler.close_output()


class SIPHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    dicc = {}

    def handle(self):

        string = ""
        # Recoge los datos del cliente:
        for line in self.rfile:
            string = string + line.decode('utf-8')
        ip = self.client_address[0]
        puerto = self.client_address[1]
        paraimprimir = string.split('\r\n')
        peticion = paraimprimir[0]
        escribir_recibo_sip(peticion, ip, puerto)
        method = string.split(" ")
        if method[0] == "SIP/2.0":
            method = 'ACK ' + usuario_sip + ' SIP/2.0'
            self.socket.sendto(bytes(method + '\r\n', 'utf-8'), ('0.0.0.0', PORT))
            escribir_envio_sip(method, ip, puerto)
            recibir_rtp()
        if method[1] == '400':
            print('SIP/2.0 400 Bad Request')
        if method[1] == '405':
            print('SIP/2.0 405 Method Not Allowed')




def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    # Definimos los argumentos que se utilizan para ejecutar el cliente
    try:
        # queda limitar los sip que esten bien formados con @
        datos_servidor_sip = sys.argv[1]
        ip_puerto = datos_servidor_sip.split(':')
        ip_servidor_sip = ip_puerto[0]
        port_servidor_sip = ip_puerto[1]

        user_cliente_sip = sys.argv[2]
        lista1 = user_cliente_sip.split(':')
        direccionsip_cliente = lista1[1]
        if lista1[0] != 'sip':
            sys.exit(
                "Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>")

        user_servidorrtp_sip = sys.argv[3]
        lista2 = user_servidorrtp_sip.split(':')
        direccionsip_servidorrtp = lista2[1]
        if lista2[0] != 'sip':
            sys.exit(
                "Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>")

        tiempo_sip = sys.argv[4]
        fichero = sys.argv[5]

    except IndexError:
        sys.exit("Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>")

    try:
        with socketserver.UDPServer(('0.0.0.0', 6003), SIPHandler) as serv:
            tiempo = time.strftime("%Y%m%d%H%M%S",
                                   time.gmtime(int(time.time()) + 3600))
            print(tiempo + " Starting...")
            rtp_port = serv.server_address[1]
            register(rtp_port)
            threading.Thread(target=serv.serve_forever).start()
            time.sleep(int(tiempo_sip))
            despedirse()
            serv.shutdown()
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")


if __name__ == "__main__":
    main()
